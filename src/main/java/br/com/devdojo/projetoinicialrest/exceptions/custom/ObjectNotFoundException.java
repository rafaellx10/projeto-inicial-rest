package br.com.devdojo.projetoinicialrest.exceptions.custom;

/**
 * Created by William Suane on 11/15/2016.
 */
public class ObjectNotFoundException extends Exception {
    public ObjectNotFoundException() {
        super("Object not found");
    }
}

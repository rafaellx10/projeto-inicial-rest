package br.com.devdojo.projetoinicialrest.exceptions.handlers;

import br.com.devdojo.projetoinicialrest.exceptions.custom.ObjectNotFoundException;
import com.google.gson.Gson;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by William Suane on 11/15/2016.
 */
@Provider
public class ObjectNotFoundExceptionHandler implements ExceptionMapper<ObjectNotFoundException> {

    @Override
    public Response toResponse(ObjectNotFoundException e) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(new Gson().toJson(e.getMessage()))
                .build();
    }
}

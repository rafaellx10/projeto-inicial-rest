package br.com.devdojo.projetoinicialrest.persistence.daointerfaces;

import br.com.devdojo.projetoinicialrest.persistence.model.enums.Condition;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by William Suane on 11/15/2016.
 */
public interface DAO<T> extends Serializable {
    T save(T entity);

    T update(T entity);

    T findById(Serializable id);

    void remove(T entity);

    List<T> listAll();

    List<T> findByHQLQuery(String queryId, int maxResults);

    List<T> findByHQLQuery(String queryId, List<Object> params, int maxResults);

    List<T> findByAttributes(Map<String, Object> mapAttributeValue, List<Condition> conditions);
}

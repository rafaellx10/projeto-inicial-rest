package br.com.devdojo.projetoinicialrest.endpoint;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by William Suane on 11/15/2016.
 */
@ApplicationPath("service")
public class Service extends Application {
}

package br.com.devdojo.projetoinicialrest.test;

import br.com.devdojo.projetoinicialrest.persistence.model.Pessoa;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

/**
 * Created by William Suane on 11/15/2016.
 */
public class PessoaPathTest extends FunctionalTest {
    @Test
    public void basicPingTest() {
        given().when().get("pessoa/oi").then().statusCode(200);
    }

    @Test
    public void invalidNameShouldReturnBadRequest() {
        given().when().get("pessoa/buscar/mercy").then().statusCode(400);
    }

    @Test
    public void verifyName() {
        given().when().get("pessoa/buscar/Goku").then().body(containsString("Goku"));
        given().when().get("pessoa/buscar/Goku").then().body("nome", equalTo("Goku")).statusCode(200);
    }

    @Test
    public void verifySave() {
        Pessoa p = new Pessoa("Mercy");
        given().contentType("application/json")
                .body(p).when()
                .post("pessoa/salvar")
                .then()
                .body("id", notNullValue())
                .statusCode(200);

    }

}
